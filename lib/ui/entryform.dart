import 'package:flutter/material.dart';
import 'package:projectbucketlist/models/bucket.dart';

class EntryForm extends StatefulWidget {
  final Bucket bucket;

  EntryForm(this.bucket);

  @override
  EntryFormState createState() => EntryFormState(this.bucket);
}

//class controller
class EntryFormState extends State<EntryForm> {
  Bucket bucket;

  EntryFormState(this.bucket);

  TextEditingController nameController = TextEditingController();
  TextEditingController descController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    //kondisi
    if (bucket != null) {
      nameController.text = bucket.name;
      descController.text = bucket.desc;
    }
    //rubah
    return Scaffold(
        appBar: AppBar(
            backgroundColor: Colors.amber,
            title: bucket == null
                ? Text(
                    'Add BucketList',
                    style: TextStyle(color: Colors.grey[900]),
                  )
                : Text('Edit BucketList',
                    style: TextStyle(color: Colors.grey[900])),
            leading: IconButton(
                icon: Icon(Icons.keyboard_arrow_left, color: Colors.grey[900]),
                onPressed: () {
                  Navigator.pop(context);
                })),
        body: Padding(
          padding: EdgeInsets.only(top: 15.0, left: 10.0, right: 10.0),
          child: ListView(
            children: <Widget>[
              // nama
              Padding(
                padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Your BucketList",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 10),
                    TextField(
                      controller: nameController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        labelText: 'BucketList',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                      ),
                      onChanged: (value) {
                        //
                      },
                    ),
                  ],
                ),
              ),

              // desc
              Padding(
                padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                child: TextField(
                  controller: descController,
                  maxLines: 10,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    labelText: 'Description',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                  onChanged: (value) {
                    //
                  },
                ),
              ),

              // tombol button
              Padding(
                padding:
                    EdgeInsets.only(top: 20, bottom: 20, left: 30, right: 30),
                child: Row(
                  children: <Widget>[
                    // tombol simpan
                    Expanded(
                      child: RaisedButton(
                        color: Colors.amber,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Save',
                              textScaleFactor: 1.4,
                              style: TextStyle(color: Colors.grey[900]),
                            ),
                            SizedBox(width: 5),
                            Icon(
                              Icons.save,
                              size: 29,
                              color: Colors.grey[900],
                            )
                          ],
                        ),
                        onPressed: () {
                          if (bucket == null) {
                            // tambah data
                            bucket = Bucket(
                                nameController.text, descController.text);
                          } else {
                            // ubah data
                            bucket.name = nameController.text;
                            bucket.desc = descController.text;
                          }
                          // kembali ke layar sebelumnya dengan membawa objek contact
                          Navigator.pop(context, bucket);
                        },
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    // tombol batal
                    Expanded(
                      child: RaisedButton(
                        color: Colors.amber,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50),
                        ),
                        textColor: Colors.grey[900],
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Cancel',
                              textScaleFactor: 1.4,
                            ),
                            SizedBox(width: 5),
                            Icon(
                              Icons.cancel,
                              size: 29,
                              color: Colors.grey[900],
                            )
                          ],
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
