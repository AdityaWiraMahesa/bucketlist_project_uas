import 'package:flutter/material.dart';
//letak package folder flutter
import 'package:projectbucketlist/helpers/dbhelper.dart';
import 'package:projectbucketlist/models/bucket.dart';
import 'package:projectbucketlist/ui/entryform.dart';
import 'package:sqflite/sqflite.dart';
//untuk memanggil fungsi yg terdapat di daftar pustaka sqflite
import 'dart:async';
//pendukung program asinkron

class ListBucket extends StatefulWidget {
  ListBucket({@required this.nama, @required this.job});
  final String nama;
  final String job;
  @override
  _ListBucketState createState() => _ListBucketState(nama, job);
}

class _ListBucketState extends State<ListBucket> {
  DbHelper dbHelper = DbHelper();
  int count = 0;
  List<Bucket> bucketList;
  String nama;
  String job;
  _ListBucketState(this.nama, this.job);

  @override
  Widget build(BuildContext context) {
    if (bucketList == null) {
      // ignore: deprecated_member_use
      bucketList = List<Bucket>();
    }

    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(200),
          child: AppBar(
            backgroundColor: Colors.amber[400],
            leading: IconButton(
                icon: Icon(
                  Icons.keyboard_arrow_left,
                  color: Colors.grey[900],
                ),
                onPressed: () {
                  Navigator.pop(context);
                }),
            flexibleSpace: Positioned(
              child: Container(
                padding: EdgeInsets.only(top: 47),
                child: Column(
                  children: [
                    Text(
                      "BucketList",
                      style: TextStyle(
                          fontSize: 25,
                          fontFamily: "Delicate",
                          color: Colors.grey[900]),
                    ),
                    Row(
                      children: [
                        SizedBox(width: 20),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Text(
                                  "Hi, ",
                                  style: TextStyle(
                                      fontSize: 30,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey[900]),
                                ),
                                Text(
                                  nama,
                                  style: TextStyle(
                                      fontSize: 30,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey[900]),
                                ),
                              ],
                            ),
                            Text(
                              job,
                              style: TextStyle(
                                  fontSize: 23,
                                  fontWeight: FontWeight.w300,
                                  color: Colors.grey[900]),
                            ),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(120, 0, 0, 10),
                          child: Image.asset(
                            "assets/Male.png",
                            width: 100,
                          ),
                        )
                      ],
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.grey[200],
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(30),
                              topRight: Radius.circular(30))),
                      alignment: Alignment.center,
                      height: 44,
                      width: 300,
                      child: Text(
                        "Create Your BucketList",
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.w300),
                      ),
                    )
                  ],
                ),
              ),
            ),
          )),
      body: createListView(),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.grey[900],
        child: Icon(
          Icons.add,
          color: Colors.grey[200],
          size: 26,
        ),
        tooltip: 'Add Bucketlist',
        onPressed: () async {
          var bucket = await navigateToEntryForm(context, null);
          if (bucket != null) addBucket(bucket);
        },
      ),
    );
  }

  Future<Bucket> navigateToEntryForm(
      BuildContext context, Bucket bucket) async {
    var result = await Navigator.push(context,
        MaterialPageRoute(builder: (BuildContext context) {
      return EntryForm(bucket);
    }));
    return result;
  }

  ListView createListView() {
    return ListView.builder(
      itemCount: count,
      itemBuilder: (BuildContext context, int index) {
        return Card(
          color: Colors.grey[200],
          elevation: 2.0,
          child: ListTile(
            leading: CircleAvatar(
              backgroundColor: Colors.grey[900],
              child: Icon(
                Icons.pending_actions,
                color: Colors.grey[100],
              ),
            ),
            title: Text(this.bucketList[index].name,
                style: TextStyle(
                    color: Colors.grey[900],
                    fontSize: 18,
                    fontWeight: FontWeight.bold)),
            subtitle: Text(
              this.bucketList[index].desc,
              style: TextStyle(color: Colors.grey[900]),
            ),
            trailing: GestureDetector(
              child: Icon(
                Icons.delete,
                color: Colors.grey[900],
              ),
              onTap: () {
                deleteBucket(bucketList[index]);
              },
            ),
            onTap: () async {
              var bucket =
                  await navigateToEntryForm(context, this.bucketList[index]);
              if (bucket != null) editBucket(bucket);
            },
          ),
        );
      },
    );
  }

  //buat bucketlist
  void addBucket(Bucket object) async {
    int result = await dbHelper.insert(object);
    if (result > 0) {
      updateListView();
    }
  }

  //edit contact
  void editBucket(Bucket object) async {
    int result = await dbHelper.update(object);
    if (result > 0) {
      updateListView();
    }
  }

  //delete contact
  void deleteBucket(Bucket object) async {
    int result = await dbHelper.delete(object.id);
    if (result > 0) {
      updateListView();
    }
  }

  //update contact
  void updateListView() {
    final Future<Database> dbFuture = dbHelper.initDb();
    dbFuture.then((database) {
      Future<List<Bucket>> bucketListFuture = dbHelper.getBucketList();
      bucketListFuture.then((bucketList) {
        setState(() {
          this.bucketList = bucketList;
          this.count = bucketList.length;
        });
      });
    });
  }
}
