import 'package:flutter/material.dart';
import 'package:projectbucketlist/open.dart';

//package letak folder Anda
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //judul
      title: 'BucketList',
      debugShowCheckedModeBanner: false,
      home: Open(),
    );
  }
}
