class Bucket {
  int _id;
  String _name;
  String _desc;

  // konstruktor versi 1
  Bucket(this._name, this._desc);

  // konstruktor versi 2: konversi dari Map ke Bucket
  Bucket.fromMap(Map<String, dynamic> map) {
    this._id = map['id'];
    this._name = map['name'];
    this._desc = map['desc'];
  }
  //getter dan setter (mengambil dan mengisi data kedalam object)
  // getter
  int get id => _id;
  String get name => _name;
  String get desc => _desc;

  // setter
  set name(String value) {
    _name = value;
  }

  set desc(String value) {
    _desc = value;
  }

  // konversi dari Contact ke Map
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['id'] = this._id;
    map['name'] = name;
    map['desc'] = desc;
    return map;
  }
}
