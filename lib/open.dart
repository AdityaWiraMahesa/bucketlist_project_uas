import 'package:flutter/material.dart';
import 'package:projectbucketlist/ui/home.dart';

class Open extends StatefulWidget {
  @override
  _OpenState createState() => _OpenState();
}

class _OpenState extends State<Open> {
  String nama = "";
  String job = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.only(top: 20),
            alignment: Alignment.center,
            color: Colors.white,
            child: Column(
              children: [
                Image.asset(
                  "assets/Logo.jpg",
                  width: 360,
                  height: 360,
                ),
                Container(
                  margin: EdgeInsets.only(left: 30, right: 30),
                  child: Column(
                    children: [
                      Text(
                        "What's your name?",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 24),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextField(
                        onChanged: (txt) {
                          setState(() {
                            nama = txt;
                          });
                        },
                        keyboardType: TextInputType.name,
                        decoration: InputDecoration(
                          hintText: 'Name',
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0)),
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Text(
                        "What's your job?",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 24),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextField(
                        onChanged: (txt) {
                          setState(() {
                            job = txt;
                          });
                        },
                        keyboardType: TextInputType.name,
                        decoration: InputDecoration(
                          hintText: 'Job',
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0)),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  margin: EdgeInsets.only(left: 130, right: 130),
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50),
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                ListBucket(nama: nama, job: job)),
                      );
                    },
                    padding: EdgeInsets.only(top: 10, bottom: 10),
                    color: Colors.amber[300],
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Next',
                          style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.w600,
                              color: Colors.grey[00]),
                        ),
                        SizedBox(width: 2),
                        Icon(
                          Icons.keyboard_arrow_right,
                          size: 29,
                          color: Colors.grey[900],
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
